package ru.ilnitsky.nsk.java.teconverter.console;

import ru.ilnitsky.nsk.java.teconverter.core.controller.Controller;
import ru.ilnitsky.nsk.java.teconverter.core.model.TEWConverter;
import ru.ilnitsky.nsk.java.teconverter.core.common.UnitConverter;
import ru.ilnitsky.nsk.java.teconverter.core.common.View;

/**
 * Класс консольное приложение.
 * Создаёт модель, представление и контроллер, и запускае представление view.
 * Created by UserLabView on 02.03.17.
 */
public class ConsoleApplication {
    public static void main(String[] args) {
        UnitConverter model = new TEWConverter();

        try (View view = new ConsoleView(model)) {
            Controller controller = new Controller(model, view);
            view.addViewListener(controller);
            view.startApplication();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
