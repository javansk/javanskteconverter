package ru.ilnitsky.nsk.java.teconverter.swing;

import ru.ilnitsky.nsk.java.teconverter.core.common.UnitConverter;
import ru.ilnitsky.nsk.java.teconverter.core.common.View;
import ru.ilnitsky.nsk.java.teconverter.core.controller.Controller;
import ru.ilnitsky.nsk.java.teconverter.core.model.TEW2Converter;

/**
 * Класс приложение с интерфейсом Swing.
 * Создаёт модель, представление и контроллер, и запускае представление view.
 * Created by Mike on 28.01.2017.
 */
public class SwingApplication {
    public static void main(String[] args) {
        UnitConverter model = new TEW2Converter();

        try (View view = new FrameView(model)) {
            Controller controller = new Controller(model, view);
            view.addViewListener(controller);
            view.startApplication();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
