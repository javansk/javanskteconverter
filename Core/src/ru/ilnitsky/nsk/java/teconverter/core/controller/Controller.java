package ru.ilnitsky.nsk.java.teconverter.core.controller;

import ru.ilnitsky.nsk.java.teconverter.core.common.Unit;
import ru.ilnitsky.nsk.java.teconverter.core.common.UnitConverter;
import ru.ilnitsky.nsk.java.teconverter.core.common.View;
import ru.ilnitsky.nsk.java.teconverter.core.common.ViewListener;

/**
 * Презентер
 * Created by UserLabView on 27.01.17.
 */
public class Controller implements ViewListener {
    private UnitConverter model;
    private View view;

    public Controller(UnitConverter model, View view) {
        this.model = model;
        this.view = view;
    }

    @Override
    public void needConvertValue(double value, Unit unit) {
        model.set(value, unit);
        view.onValueConverted(model.getAll());
    }
}
