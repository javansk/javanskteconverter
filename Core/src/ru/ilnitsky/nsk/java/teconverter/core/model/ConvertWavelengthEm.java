package ru.ilnitsky.nsk.java.teconverter.core.model;

import ru.ilnitsky.nsk.java.teconverter.core.common.Unit;

/**
 * Класс преобразования температуры в длину волны (м) фотона с энергией равной тепловой энергии
 * Created by Mike on 27.01.2017.
 */
public class ConvertWavelengthEm extends ConvertAbstract {
    public ConvertWavelengthEm() {
        super("Длина волны излучения,   м", Unit.WAVELENGTH_E_M);
    }

    @Override
    public double toK(double value) {
        return Functions.to_K_from_WavelengthEm(value);
    }

    @Override
    public double fromK(double k) {
        return Functions.to_WavelengthEm_from_K(k);
    }

}