package ru.ilnitsky.nsk.java.teconverter.core.model;

import ru.ilnitsky.nsk.java.teconverter.core.common.Unit;

/**
 * Класс преобразования энергии для Дж
 * Created by Mike on 27.01.2017.
 */
public class ConvertEnergyJ extends ConvertAbstract {
    public ConvertEnergyJ() {
        super("Энергия,    Дж", Unit.J);
    }

    @Override
    public double toK(double value) {
        return Functions.to_K_from_J(value);
    }

    @Override
    public double fromK(double k) {
        return Functions.to_J_from_K(k);
    }

}
