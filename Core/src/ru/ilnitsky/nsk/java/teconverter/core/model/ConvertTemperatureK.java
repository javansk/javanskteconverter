package ru.ilnitsky.nsk.java.teconverter.core.model;

import ru.ilnitsky.nsk.java.teconverter.core.common.Unit;

/**
 * Класс преобразования температуры для K
 * Created by Mike on 27.01.2017.
 */
public class ConvertTemperatureK extends ConvertAbstract {
    public ConvertTemperatureK() {
        super("Температура, К", Unit.K);
    }

    @Override
    public double toK(double value) {
        return value;
    }

    @Override
    public double fromK(double k) {
        return k;
    }

}
