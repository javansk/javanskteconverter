package ru.ilnitsky.nsk.java.teconverter.core.model;

import ru.ilnitsky.nsk.java.teconverter.core.common.ConvertUnit;
import ru.ilnitsky.nsk.java.teconverter.core.common.Unit;

/**
 * Абстарктный класс для перевода величин
 * Created by UserLabView on 27.01.17.
 */
public abstract class ConvertAbstract implements ConvertUnit {
    protected String description;
    protected Unit unit;

    protected ConvertAbstract(String description, Unit unit) {
        this.description = description;
        this.unit = unit;
    }

    @Override
    public Unit getUnit() {
        return unit;
    }

    @Override
    public String toString() {
        return description;
    }
}
